---
date:
title: "About"
type: "about"
layout: "section"
---

After twenty years in Vermont and New Hampshire<!--[Vermont and New Hampshire](nitty_gritty#the-beginning)-->, I skipped around for a decade of teaching in the Marshall Islands<!---[Marhsall Islands](the-marshall-islands)-->, Vermont<!--[Vermont](nitty_gritty#vermont)-->, Bhutan, and Vermont Again, intermingled with some volunteering in Vietnam, Japan, Thailand, Myanmar, France, Portugal and Spain. Now I'm studying Machine Learning at Aalto University in Finland. <!--Hopefully, that's where it starts to get [interesting](nitty_gritty#the-future).-->
