---
title: "Fab Academy"
date:
draft: true
---

## Welcome to my Fab Academy site for 2021.

Here's the plan: I want to make a pocket-sized tool for learning and practicing the bass clef and treble clef notations for piano. The device will open with a single octave of keys on one side and a screen on the other. With the keys you select the clef, the musical key (ie., the scale) and the speed. The device then starts sending notes at that speed and you try to keep up. It continues until you get three strikes, then you get your score.

[Here's a first sketch of the idea.](/fab/assignment1)
