---
title: "Bhutan Update #4"
slug: "bhutan-update-4"
date: 2016-02-29
draft: false
categories: [travel]
tags: []
favorite: false
---

*Note: this was originally sent from after leaving Bhutan the first time. I don't agree with everything I said back then, but I'm pasting it here unedited as a kind of time capsule.*

Dear friends-that-I-hope-to-see-soon,

This is my final email from Bhutan, and it’s a short one. So much has
happened in the past few months—so much more than I can share! By the
numbers: my journal words-per-day has climbed from the low 100s in the
spring to 274 in December, 575 in January and 741 in February. And not
because I’ve had more time for complex prose. In fact, no modifier is
safe any more.

I have tried, in previous emails, to give colorful specifics. But this
time, there’s just too much. So, I will abandon specifics and give
only the briefest summary; the specifics will have to wait till I see
you. I hope this email will simply explain why I have continued to be
out of touch. Some of you may have thought I was already in America
and ignoring you. Not so! In fact, I am in India and ignoring you.
Just kidding, I am in India, and I have had no reliable way to
communicate.

The last three months of school (September to November) are a bit of a
blur now, a mostly-happy blur of festivals, apple picking, picnics,
walks, and projects (I made four short movies with my students, which
I will share when I have better internet). The fly in the mostly-happy
ointment was exams. Exams are everything in Bhutan, which makes Fall a
busy time. The other teachers didn’t stress much: they gave the
students “trial” exams the week before. These had the exact same
questions as the real exams, which, as you can imagine, makes it
pretty easy to study. This solved a long-standing mystery for me: how
will the students pass the other subjects if the teachers stopped
teaching four months early?

In December, my parents visited me! This was also a blur, because we
never stopped moving. We found a mountain to climb and a ritual to
crash almost every day. No rest for the weary. My philosophy was, with
only three weeks to experience Bhutan, we can see it now and process
it later. I don’t claim that it’s a very sound philosophy. Anyways, we
stayed a week in Tsebar and then traveled for two weeks, spending time
with 8 other teachers in 9 other districts.

January: His majesty’s Winter Youth Engagement camp for “vulnerable”
girls. I know, vulnerable is an icky word. Let me tell you what it is
supposed to mean and what it really means. Vulnerable is supposed to
mean the poorest girls in Eastern Bhutan; the ones from the most
remote villages and the most broken families, who are most likely to
end up overworked and underpaid on road construction crews during
their winter holidays. And that’s all true. At the end of camp, when
someone read aloud the statistics about our students, I cried. Having
fallen in love with these students, it was painful to hear that 20%
are orphans and 50% have experienced domestic violence.

But none of that is what vulnerable really means. (Touchy-feely alert)
What it really means is trusting and loving and caring to a fault.
Because what makes us vulnerable is opening our hearts. And what the
other volunteers and I learned is that opening up is often worth the
risk and the pain. Thanks to our amazing students at camp, we learned
how fast it is possible to connect with other humans if you show them
love and trust from the beginning. We connected with them and with
each other like I never have before. I would not have thought that it
was possible to feel so close to anyone after only three weeks. I am
eternally grateful for that lesson.

After camp finished, my friend Alex and I realized that we now had
friends in hundreds of villages in eastern Bhutan, so we set off to
visit them. For three weeks, we walked and hitchhiked to the most
remote parts of the country. We stayed with students and experienced
boundless generosity in the most breathtakingly beautiful places.

I’ll leave it at that for now. I would love to write about each of
these things in detail, but I can’t see the narrative thread right
now. Someday, if I find it, you might get another email from me. Thank
you so much for taking the time to read this email and its loquacious
predecessors. I hope they have been interesting!

Love,
Dan(iel)
