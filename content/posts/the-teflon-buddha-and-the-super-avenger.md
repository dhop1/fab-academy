---
title: "The Teflon Buddha and the Super Avenger"
date: 2021-02-16
draft: true
---
Why is it hard for Western people to become more buddha-like? Well, there's practice for one thing--and it's a big thing--but there's also a nagging part of our brain that refuses to accept the change. Because here's the thing: to be more buddha-like, you need to stop judging. Everyone and everything, even bad things. And that means throwing away the strongest motivation to make changes that we know of. Giving up on your internal critic means giving up on your ambition to change the world.

I'll give you an example. When I was teaching in the Marshall Islands and Bhutan, I worked hard to show up with all my energy every day. I never missed a day of class in several years of teaching. I couldn't--how could I leave those kids sitting in an empty classroom when I was their best hope at learning something?

The flipside is that I was continually annoyed at the other teachers for being so goddamned lazy. Some teachers left third graders alone in  classrooms for entire days or weeks without visiting once. The students never held it against the teachers, both because they're used to it and because they're far more buddha-like than I will ever be. So I shouldered their share of annoyance as well as my own. I was a pretty grumpy member at staff meetings and staff parties, and I avoided the staff room like a plague den.
