---
title: "Bhutan Update #6"
slug: "bhutan-update-6"
date: 2018-05-18
draft: false
categories: [travel]
tags: []
favorite: false
---

*Note: this was originally sent from my posting in Gongthung, Tashigang, Bhutan. I don't agree with everything I said back then, but I'm pasting it here unedited as a kind of time capsule.*

My update letter is attached. It is looong; please amortize your annoyance over the past 15 months of update-free living. Also, for more audio-visual types, here is a link to a short, hodgepodge of a video showing some of the places described.

{{< vimeo 271212997 >}}

## E-mail

Dear Friends and Family,

Before I begin, and apropos of nothing, am I the last person in the world to realize (today only) that a cow’s knee is actually it’s ankle?
![Cow Knee](/media/bhutan/cowknee.jpg)

Now, where to start after more than a year without a letter? Let’s skip lightly over the usual apologies and promises to be better (better is a pretty low bar for me anyway) and go straight to a few anecdotes. I will try to keep my philosophizing tendencies in check until the end, when only the most dedicated might still be reading.
Last week, Bhutan surprised me again. One of things that I have waxed hyperbolic about in the past is how wonderful it is to look at the glinting tin roofs dusting distant ridges and know that I will be welcomed warmly into all those homes. That is a striking difference from America.
But close community is not all rainbows and sunshine. It can turn ugly when it feels threatened. I have seen heartbreaking examples of this when a village has decided, because of an accident, that one of its own is a “poisoner” and refused forever after to take her hospitality. But I never expected that I would get on the wrong side of it.
### Khekpa
Recently, the ridges and valleys of Eastern Bhutan have been stricken with khekpa fever. A khekpa is a human hunter who steals people to be buried alive to appease local deities. Khekpa rumors (and maybe reality) are as old as history here, but they are very heated this year thanks to social media.
Last weekend, I went for a visit to the next ridge over from ours, a string of remote villages called Narang. It was a beautiful walk there and I munched on wild strawberries, yellow raspberries, wintergreen, wild pepper roots, and cinnamon and collected mushrooms and fiddleheads on the way. The next day, while my friends were busy performing a ritual for a dead relative, I decided to walk back alone. They warned me that the Narang villagers are fearful of strangers and armed with poison arrows. Apparently, they are trigger-happy after losing villagers to khekpa in 2009 and 2011. I promised to stay on the road and stubbornly prevailed.

So I set off and soon found myself shouting to every fearful villager, “Khekpa mangi! Not a khekpa!” (Yeah sure, a khekpa would say that). After passing the last house in Narang, I encountered one older woman returning home with her cows. When she saw me, she hitched up her kira and booked it into the jungle, running like no fifty-year-old woman has ever run in Bhutan. I shouted my denials after her, but she did not answer. I only saw glimpses of her disappearing up the mountain.
With no other option and eager to remove myself from arrow range, I continued my journey, wary of the shouting now coming from the hillside behind me. I only learned later what happened next. A truck happening to pass by Narang was deputed to interrogate me. Unfortunately, I went off the road for a pee break exactly when the truck went by. The driver called the villages at both ends to assure them that yes, here was a confirmed headhunter hiding in the jungle. Both villages dispatched armed hunting parties, eight from behind me and an unknown number from ahead.

I don’t know how close I came to meeting one or both posses, but it can’t have been far; the total walk between the two is only thirty minutes. Social media saved me this time. The gossip moved so quickly that my friend’s mother (whose house I had slept at the previous night) got a call minutes later from someone a hundred kilometers away. She figured out what was going on and managed to call off both groups.
Lesson learned: peoples’ absurd gullibility has real consequences aside from comically urgent WeChat posts about the imminent extinguishing of the sun (confirmed by “NASA” and reposted every year without fail by at least one teacher).

Follow-up: The fear-level is getting so high now that I’m afraid walking around my own village. Everyone is armed and moving in groups. People talk breathlessly about unrecognized cars. Rocks are hitting the roofs of houses and our student hostels, apparently a proven khekpa technique to lure out victims. The only other person who is not scared is my friend’s grandfather, always my favorite source of pre-modern stories and peacefulness. But his reassurance was not very reassuring: I asked him, “are you scared?” He laughed and said no, “The government is not building any big projects right now, why would they need bodies? Well, except the khulongchhu hydroproject...”

### Fire
The past year has not been uneventful either. We started off last year with a massive forest fire that crept up from the valley all afternoon and then roared past the village late at night. That incident found our students (and yours truly) strung out over a thousand vertical feet of oily lemongrass forest beating the fire with green branches, then running for our lives every time a gust flamed it back up to twenty raging feet.

That fire gave me a vanishingly rare romantic opportunity—making tea for a nurse who was home on holiday while assuring, from extensive experience, that the fire could not cross the maize fields below our houses—and then promptly took it away when the fire did indeed cross the maize fields to rain burning vegetation on us.

### Water
Last summer found us without water for a few weeks. I think I have previously mentioned this irony of life in Bhutan: when the monsoon rains come, the pipes from distant water sources get blocked. Besides turning the school hostel toilets into a vision from janitorial hell, it is very inconvenient living without nearby water. When all my clothes were too dirty even for my own exceedingly low standards, I had to hike three kilometers to find an unblocked pipe. Thirty years ago, that was every day life here on this dry ridge. Back then, this place was so poor that when the queen visited, they were only able to offer her a pot of dirty water hauled from far away. Apparently, the queen removed her jewelry on the spot and gave it to them to buy water pipes.

### Wind
Another adventure that is worth getting a little poetic for was a hike up to the highlands last winter. Throughout the year, I was greeted every morning by the 4000 meter peak across the valley, the first in a chain stretching up to the high Himalaya. I finally got a chance to go in December, when my adventure buddy Alex came to visit. We brought one student and his uncle and the four of us made the two day hike up to that peak.

The poetic part was the chance to see the world, briefly, from the highlanders’ perspective. The majority of Bhutanese are in a few dominant culture groups and live between 1500 meters and 2500 meters. That is where breakneck development is cutting its exponential swath across the landscape. Where smartphones and TVs have, in the past few years, made their presence felt and started to change the once-so-relaxed and accepting approach to life. Where, also, the way of life seems to have leapfrogged a decade or two even since I reached here.
But the highland brokpa people stay aloof from that world. In winter, they bring their cow and yak herds down to jungle huts at about 3000 meters and in summer they stay in meadows at 4000 meters. From there, they look down the precipitous, cloud-forested slopes at the hazy world so far below and it seems mostly irrelevant.

Each brokpa settlement is a self-contained world. One or two husbands with one or two wives, each with two or three children living in a bamboo shelter surrounded by a large herd and few watch dogs. It feels infinitely far from the choices that roads and electricity bring to every moment of our lives. Life is slow, measured by the days it takes to cross the rocky ridges and visit friends and family in distant huts and the central village, where students and grandparents stay.

Their world is also a physical inversion of ours below. Where we drive or hike hours and hours following contours around mountain spurs, they occupy those negative spaces, all joined to central peaks. Their world is the simply-connected topology that, cut out from the world below, leaves it a confusing mess of valleys, like a slice of brain from a CT scan.

At the top of the mountain, we found a small brokpa hut open and already stocked with firewood for chance visitors. We raced the darkness and cold to collect water from snowmelt, build a fire and cook dinner. Meanwhile, we tried one of the two accepted altitude-sickness preventions: alcohol. I can’t speak for the effectiveness of the other (sugar), but this fix left me with a headache for a week after.

In the morning, we followed the ridge for a few kilometers to one of the holiest sites in eastern Bhutan, a pristine green lake nestled in a forested saddle. This lake is the reservoir for all the valleys radiating outward. Its deity is considered the most powerful and dangerous in the region and people are not allowed to visit for half the year. Any disturbance could bring deadly storms. We stayed for a peaceful hour, then filled our bottles with holy water and descended slowly to a different valley, me nursing my headache and Alex stepping tenderly on a world-class blister.

### Earth
My main goal here has been trying to dig more into daily life. I can speak the language pretty well now and I never tire of chatting with the grandparents. They grew up in a world with slavery, feudal lords, severe food shortage, no changes of clothes, a two-week roundtrip to buy salt, and an hour of hauling water every day. They would never meet people from facing villages because of impassable rivers. They could only communicate with those people by asking a lay monk to write a letter and then tying it to an arrow and shooting it across the river. They have experienced the world during a monotonically upward trajectory, and many of them radiate an unmatched and unmatchable sense of calm.

But the bulk of my daily-life immersion has been working in the fields, shoulder to shoulder with the villagers. Ninety percent of the work is backbreaking: sometimes hauling manure or potatoes but mostly hacking at the soil with a short spade. It exposes the good and bad of development in short order: the work is tough and I am usually happy that I never have two consecutive days off school in which to suffer, but the mutual support is amazing. Because it is so slow and difficult, villagers move together to a different field every day and take lunch, dinner and several alcohol or tea breaks from the day’s host. The best communal work is the day when a new house receives its mud roof layer. It’s a ritual day when thirty or forty people will come to spend hours stomping on or dancing on or playing in the mud to compact it before it hardens in the sun, all followed by drinking and more dancing.

Another feature of (not quite daily) life here is frequent exposure to death. People die a lot more unexpectedly than they do at home. Add to that a very intertwined web of familial and neighborly connections and we end up at a lot of funeral rituals. Death is perhaps the occasion when communal life shines most. Instead of leaving people alone to “grieve”, the family of a dead relative is not left alone for a minute during the first 21 days. Villagers come in a constant stream, sometimes bringing small gifts and always expecting drinks and meals (which keeps the bereaved productively busy). I have slept over a few times at these houses and it feels more festive than anything. I have seen my students laughing at jokes even a day after losing their mother or father. (The frankness towards death gets a little bizarre sometimes: people will loudly tease others for crying after losing their husband or daughter. Also, the same day that our student’s mother died, teachers were joking about getting free rent at her house.)
One reason for my immersion project (which also included a three-month stretch without eating dinner at my own house), is to try to cure myself of a western affliction that I have a particularly acute case of: romanticism.

My working definition of romance (please quibble) is selectively emphasizing or suppressing details to paint an appealing picture. I mean, for example, I am able to see a village five hours from the road, living off the land, and sob from the beauty without seeing the suffering. I can see horses peacefully grazing and ignore them painfully hauling potatoes over mountains for sale. I can see people working a field side by side with friends and neighbors, laughing and singing, and ignore their aching backs, cracked feet, wrinkled skin, and the alcoholism that gets many through it. I can see families sleeping together in a cozy pile and fail to consider the insects that perpetually assault them. My heart aches for the beauty of steep mountains and relegates the suffering they cause. I hear about the brutality of the past and yet I still yearn for it.

So, after more than two years, I have to ask myself, is my case getting less or more serious? My goal was to reach the point where I would see even a “Microwave-cooking-for-one” future as an improvement on past hardship. I’m not close to that, but I also don’t scorn development as I once did (or rather, bit my tongue to avoid doing). Basically, I’m still stuck firmly in the vast canyon of cognitive dissonance between the two.

### What about school?
Huh, more than 2,000 words and I didn’t mention my job. Anyway, it’s all fine, if a little too constricted on the academic front. My math and physics teaching is on point these days, and kids here continue to be pretty awesome. Also, I wrote and helped direct a Bhutanized version of Matilda last year for a drama competition, with special effects! Teaching is still the only job I know where I can take turns as a storyteller, singer, dancer, engineer and movie maker. But...

### What next?
Great question, what do you think? You have a collective few centuries of grown-up-ness under your belts, what’s exciting and challenging and satisfying in the world these days?

### What about that money we gave you?
It’s mostly distributed, but isn’t this letter long enough? Let’s talk about that next time.

Please send your updates. I am, as always, an eager recipient of any and all news.

With Love,
Dan
