---
title: "Bhutan Update #1"
slug: "bhutan-update-1"
date: 2015-04-15
draft: false
categories: [travel]
tags:
favorite: false
---
*Note: this was originally sent from my posting in Tsebar, Pemagatshel, Bhutan. I don't agree with everything I said back then, but I'm pasting it here unedited as a kind of time capsule.*

Life here is by turns beautiful, frustrating, relaxed, formal, healthy and perilous. And always surprising. I have been repeatedly blindsided by kindness, weird and wonderful cultural differences, and godawful teaching. I
have been brought to tears by the sight of a pristine landscape dotted
with centuries-unchanged villages. I have been brought to near
violence by our boys’  warden, a former monk who takes sadistic
pleasure in hitting children and kicking dogs. On one occasion, I
watched him shouting and throwing heavy metal chairs at students for
an hour during dinner. I have never been so ready to hurt someone, but
throughout it all, he wore a protective baby on his back. There were
no consequences. Power corrupts, and teachers here are all powerful.

The flip-side, which more than compensates, is that the students are
as lovable as possible. They are all from farming families, and they
are all tough, uncomplaining, and eager to learn. Last week, after a
volleyball game, I pointed out to a student that the first star up was
actually Venus. A crowd of students gathered to ask questions about
space, and we continued on tangents for more than an hour. And all of
them skipped dinner to hear the answers. That’s a level of curiosity
that even I can’t claim. They are also totally self-sufficient. The
students run the school and take care of all the younger kids with
compassion. They organize everything: inter-house badminton,
volleyball, ping pong and debate contests, gardening, cleaning, and
even taking attendance and keeping time for the school day.

It’s difficult to give an example of a typical day here, I think we’ve
only made it through a full week of school once. (Other weeks have
been cut short by various, usually unannounced, occasions. Some
examples: We had a four-day retreat at a Monastery where I said the
same sentence 16,000 times. We had a three-day local festival that
involved lots of dancing, lots of gambling, and the crudest public
penis humor I have ever seen. We had a day where we just played games
outside while monks sat inside blessing our facilities. We had a
two-day school fete, where I built and hosted an airsoft shooting
gallery.)

School always starts with an assembly, featuring meditation, prayer,
singing and a rotating teacher-on-duty spouting some hypocrisy about
the importance of hard work or punctuality. This to students who wake
up before five, have about two free hours each day and are required to
stay in the classroom all day when their teachers decide that the
weather isn’t agreeable for teaching today. (I am guilty of some
preaching as well, but my speeches tend towards the positive. And
usually include a slam poem :) Sometimes I miss the Marshalls, where
the anarchy went both ways: teachers never went to class, but at least
students gave us a hard time also. Then I remember how stressful that
was, and I don’t actually miss that. There are zero discipline
problems here. Or if there are, the students deal with them.

After assembly, I teach until four-ish. Class 8 and 5 “maths.” On
paper, I have plenty of free periods, but in practice I am doing a lot
of unofficial subbing for teachers who have better things to do (maybe
chewing doma, maybe text-flirting with someone other than their wife).
Some students understand everything and eagerly finish pages that I
haven’t assigned, others can’t add one-digit numbers. The middle is
pretty hollow. That stresses me out sometimes, but then I remember
that this country really values its culture and that culture requires
a lot of farmers.

I’ve never taught with a textbook before, so I’ve never felt driven by
the need to finish a curriculum. Now I constantly feel the pressure.
But the upside of other teachers not teaching is that there are plenty
of free periods for more fun things: I am reading Roald Dahl books to
all of my classes (Matilda is wonderfully subversive here), we sing
every week, and I have taught most of my favorite games: mafia,
thumper, that game where you shoot and reload, lots of card games (Set
is really popular), and Zoom, Schwartz (changed to "zoom, solo, magi,
tsebar" (which translates to "seven, chili, instant noodles, tsebar”
(the last is my village))).

After school, we play badminton or volleyball at school or frisbee or
futsal on the tiny field behind my house (the only flat place for an
hour in any direction). Or I take a walk with some of the day
students. These walks are very stop and start. The older kids share
from there seemingly endless wisdom about the natural world, and show
me how to find all kinds of different edible pieces of nature. The
younger ones use their only english phrase over and over and over:
“This is _____, this is ______, this is…”. We often end up being
invited into someone’s house, maybe a student’s family or more likely
a total stranger, for tea or bangchang. Bangchang is the local maize
wine. It’s delicious and powerful, and I will bring it home if I can
master the recipe.

They drink a lot here (maize is the dominant crop and I think it
mostly goes to making various homebrews). It took me a long time to
learn to drink only a moderate amount (none is not an option, this is
a culture of militant giving). When you have had enough (after the
mandatory “second-share”), you must be completely stone-faced in your
refusal. If you crack the briefest smile or make the slightest eye
contact, you will be stumbling home drunk. And this is not a place to
be stumbling anywhere. Every path is extremely high-consequence,
dropping precipitously on one side, if not both.

I’m happy to say that this giving spirit does rub off even on a
calculating american materialist. It’s difficult to give here, since
they will always say no at least three or four times before accepting.
But I am getting the hang of it. The trick is to get it in their hands
before they can refuse. You can throw something at them and run, or
drop it in their gho pocket, or hide it in a seemingly empty
container.

Now, about the place. This country gives you the sense that there are
mysteries to discover around every corner. Crossing Bhutan, we were
amazed entering every valley: Bhutan is a kaleidoscope of different
climates, different topographies and different flora and fauna,
transitioning unpredictably from valley to valley. Which adds up to a
super-abundance of wanderlust, and frustration about the impossibility
of seeing more than a tiny fraction of those valleys. Before coming, I
thought Bhutan was a small country. After spending five days crossing
it, it felt huge, but my little district still seemed small. Now, even
Pemagatshel seems huge, because it is so difficult to reach some of
the valleys, and I have no idea what is over some of these hills.

We only have Sunday off (and not always that), so it is difficult to
adventure. We had one long weekend in April, and me and my nearest
native-English-speaking neighbors (an American and a Canadian who live
across the valley, a short 4 hour jaunt away) decided to attempt a
trip to the neighboring district, Mongar. Way back in Thimphu, I had
seen a dotted line on a map, indicating a footpath connecting the two
districts. As soon as I saw that line, I had to try and visit our
friends in Mongar. I asked everyone I met whether the trail was still
passable, and the vote was split. People tend to say that trails don't
exist until you get to the neighboring village, and then they say:
sure, go that way.

Anyway, we set off at 5am, planning to cover the
40km in one day. Hah. The "trail" was a river, which we crossed 23
times between steep mountain walls on both sides. Then we got lost.
Then we had a four hour climb straight up. Then it was evening and we
were less than halfway. We spent the night in a little village called
Khengkhar, where a few school staff played hosts and tour guides for
us. We also saw the ugly side of being very generous. One man became
very drunk and was deeply offended that we wouldn't stay at his place.
He spent hours shouting at us for not trusting him. Adam and I (the
Americans) mostly ignored him, so he became especially enamored with
Mike (the Canadian). He said things like, "You two can stay here, but
I am taking the Canadian," and "I'm going to make Mr. Canada happy all
night long..." So, yeah, that was awkward, and we got up and left
Khengkhar as early as possible.

We were discouraged about our progress, and we thought about walking a
little and then getting a vehicle, but the road was washed out six
places in the first kilometer from the previous night's rain. So,
without a choice, we just kept walking. This day was on a high,
coniferous plateau, completely unrecognizable from the jungle valley
where we live. We made it to Mongar that night, stayed over with our
friend Nakita, and spent the whole next day hitchhiking home (the
other guys hitchhiked home, I hitchhiked to a gypsum mine, which still
left me 6 hours of walking in eerie, demon-infested darkness).

The monsoon has started now, and with it the leeches, so long hikes
are a little less appealing. But we have a couple on the agenda for
our break in July.

So, for now, school life goes on as normal. Except that most of the
teachers, having just gotten into the swing of things, have decided
that it's time to stop teaching and prepare exam "blueprints" for next
month. So the kids are on their own again to learn and get beaten if
they somehow fail to learn enough without a teacher.

Other tidbits:

*Prayer wheels*: Spinning prayer wheels and the accompanying bells are
an omnipresent fact of life here. The one outside my house usually
starts spinning at 5 and doesn’t stop until around 10. Everyone takes
a turn or two when passing, and it is a retirement plan for some of
the older villagers. For those of us who don’t necessarily believe in
accumulating karmic merits this way, it still serves as an excellent
reminder: pausing and taking a moment for the well-being of all
sentient creatures every time you walk between the staff room and the
classroom can’t hurt!

*Showers*: I can’t actually remember my last shower (and by shower I
mean dumping cold water on my head). Going on two weeks I think. And
they have been pretty sweaty weeks. I always believed, but never had
the courage to confirm, that the body would reach some kind of
equilibrium eventually. I think I have reached it. Or maybe I smell
and look terrible. Either way, it’s pretty nice.

*Mirrors*: This might go hand-in-hand with the tidbit above. I have
only seen one or two mirrors since arriving in February. And, I have
enjoyed their absence so much that I avoided looking at those. I can
feel my sense of self changing gradually shifting to focus more on the
inside than the outside. I think it is a small factor in the kindness
and well-balancedness of the people here.
