---
title: "Bhutan Update #2"
slug: "bhutan-update-2"
date: 2015-07-01
draft: false
categories: [travel]
tags:
favorite: false
---
*Note: this was originally sent from my posting in Tsebar, Pemagatshel, Bhutan. I don't agree with everything I said back then, but I'm pasting it here unedited as a kind of time capsule.*

A few things have changed since a
month ago. The monsoon is upon us. Some students helped me plant a
garden. Old school British-style exams were the worst (being the enemy
in a room of two-hundred students trying to cheat because their future
depends on a couple of questions that they were never taught the
answers to). The time before exams was the best (no one was teaching
so we did lots of singing and reading in class). And now it’s break!
All of the foreign teachers are meeting at a hotel near me for two
days where we will hatch plans for two weeks of adventures. I have
challenged five other teachers to a rap battle when we reach the
hotel, and I have prepared four pages of grade-A burns. The rhymes are
good, but I’m not so confident about the delivery. I will make sure
there is a video and then I will either share it with you or bury it
so deep on my hard drive that it will never see the light of day.

The theme of this second letter is: *Things that make you a good
buddhist if they don’t drive you insane first.* Or: *Things that kind of
suck but maybe that’s healthy.*

1) Prayer wheels, as mentioned before. This nearly did me in at the
beginning, since I am right next to the loudest one around. I
considered several un-buddhist countermeasures: putting some sand in
the grease or wrapping a damper around the bell. But the sound has
grown on me, and now I am happy to be the first recipient of these
blessings. They are for all sentient beings, but they must dissipate
as the inverse-square of distance, so it’s good to be close.

2) Dogs. Much worse than prayer wheels. They travel in big gangs and
have gang bark-outs outside my house at all hours of the night, and I
don’t get the feeling they are barking for the benefit of any other
sentient beings. This is one more chance to be like the locals and
just hope I don’t get reincarnated as a half-starved stray in a lesser
dog gang.

3) School. We had a four hour meeting last week, which I can summarize
in a sentence: we will meet in subject groups after each exam to do
group evaluation of the test papers (Why? No one knows). [Note: this
was a TERRIBLE plan. Everyone got drunk before marking my tests, so
they’re covered in sloppy red ink and I had to do them all again.] The
students sat in their classrooms the entire meeting doing nothing. I
am constantly reminded of a story that I read in a Feynman book. He
described pacific islanders trying to lure cargo planes back to their
islands after the war by setting up mock runways and mannequins with
torch batons. He called it cargo-cult science. School here is
cargo-cult science: the teachers believe that if everyone shows up to
school every day and does the correct paper-work (action plans and
block plans and lesson plans and plan plans), the students will
somehow learn. Never mind the crucial final step: going from the staff
room to the class room and speaking. Many teachers stopped teaching
entirely about two weeks before exams, saying that they had finished
covering the syllabus (as outlined in their “block plan”). Never mind
that half of the students will fail to reach the low standard of a 40%
pass on the exam, the teacher has done his job! This is the ultimate
example of the theme of this email and the closest to driving me
crazy. I won’t pretend to be the hardest working teacher in the world,
but there are few things more frustrating than working hard while
everyone around you is slacking off. The only way to survive is to
cultivate a buddhist big-picture: nothing matters; appreciate the
little things.

4) Water. Too little and too much. For months, I counted on the
kindness of neighbors to fill my bucket when water would
intermittently drip in the courtyard. Now, just when I had started to
appreciate this fact of life as an opportunity for neighborliness, we
are drowning in water. After my last email, I did not see the sun for
three weeks. For those three weeks we had drizzle every day and
torrential thunderstorms every night, tapering off just when the
students finished slogging from their villages. (These thunderstorms
give bhutan its name: land of the thunder dragon. The thunder rolls
around the valleys for so long that it overlaps with the next and
never really stops.) There is a teflon biofilm on top of the mud that
makes every trail into a death trap (these trails were beyond my
abilities when it was dry). Even crossing level paving stones at
school is a risk. (Risk is very different in a buddhist country.
People confront death (and other facts of life) much more head-on, and
it is a little less scary when you are sure to re-incarnate. When you
get into a situation that is beyond your control, e.g. riding with a
drunk driver, you mutter “om mane padme hum” and hope for a good
rebirth). And the silver lining of the monsoon is the glory of the
sunshine after three weeks without it. We have had a few days of sun
now, and it feels like a gift.

5) Archery and Khuru (darts). Archery and darts are the national games
here. They have as much in common with our archery and darts as the
monsoon does with a sprinkler. Archery is played with bamboo arrows
and a bamboo bow. The archery ground is typically two flat patches
carved into the hillside about 100-150 meters apart. The flat patches
themselves are only maybe 5 meters wide and the targets are planks of
wood about six inches wide and two feet long, i.e., difficult to even
see at that range. Even with laser-enhanced vision, I lose arrows at
an alarming rate. Darts means throwing fletched metal spikes about
thirty meters at a small piece of wood. Even the best can’t hit the
target with any regularity in either game, and you spend most of the
time watching other people shoot or walking from one end to the other.
So really, these games are a test of practice and patience. Isn’t that
what Buddhism is? Even for an impatient hedonist though, there is
something really addictive about watching arrows and darts soar
through the air. Also, drinking and machismo are a big part of these
games. Machismo is demonstrated by dancing and singing in front of the
target to taunt the shooter/thrower. One more way to die in
Bhutan…While I’m at it, a subtopic:
        A Million Ways to Die in Bhutan
                i) Already mentioned: arrows/darts, trails, roads
                ii) The jungle, including tigers, cobras, bears, and elephants
(seriously, a couple was killed by mountain elephants last year on one
of my favorite trails)
                iii) Monkeys. Frequently, when you’re passing under a particularly
unstable shale scree, where twenty tons of rock are itching to find a
lower gravitational potential, monkeys will start playing at the top.
You stop to watch because they have monkey babies, and those are
adorable, and then big rocks start to whizz past your head. Real
funny, monkeys.
                iv) Roofs. The traditional house roofs are made of big two-by-two
chunks of slate, which occasionally fall off for no good reason. The
new roofs are made of sharp tin, often jutting out right at head
level.
                v) Left-hand driving. Not a big danger here, but in the capital this
almost got me a dozen times.
                vi) Cows. Cows are not predictable creatures, and I count on their
good graces every day when I skirt around them on trails and on the
way to school.  They all have magnificent horns, and they always seem
suspicious of me (and they really hate my bike). All it takes is one
trigger-happy bull when you’re walking behind him…

6) Itching. Non. Stop. Itching. My feet and legs and arms are a mess
of wounds and scabs. And I have NO IDEA WHY. To avoid killing insects,
which in this place would feel like using dynamite to shovel the
driveway, I’m trying to use the IPM approach: welcome the lizards and
other keystone predators. The locals put out food for their stray
dogs/cats/rats/mice. If you make them strong and healthy, they will
keep others away. And it turns out that even non-stop itching is
something that you can get used to.




Section II: Things that I can’t force to fit the theme above

1) Shopkeepers. No one else thinks about this, but I always worry
about which shop to patronize. Thanks to the school and our location
on the road, we are the shopping hub for fifteen or twenty local
villages, so we have an astonishing seven shops, all selling more or
less the same strange assortment of foods. The problem is, as soon as
one gives me a generous gift (which outweighs whatever cumulatively
paltry profit margin I have given them) and I decide to patronize that
shop, another does the same. So my loyalties are increasingly torn. I
would like to give gifts back, but what do you give a shopkeeper?
(Speaking of generosity, until my language comprehension improved I
didn’t even realize the extent of it. Now I know that most of the
people that I meet out on the trails or on the road are offering me a
drink or apologizing for not having anything with them to offer. What
a difference, huh? Also, fun tidbit: shopkeepers give small change in
chewing gum, which you then share with the first group of people you
meet.)

2) Distance. I don’t think I properly conveyed how remote it is
possible to feel even when you are living on a road. The village of
Khengkhar, which we hiked to in a previous email, is in sight at the
far northern end of our valley. You can see the houses on a clear day.
But it would take at least 12 hours to drive there, if the road is in
perfect condition. You could reach Georgia from New York in that time.
And I can see the houses! (A corollary is that the top-speed driving
here is about 20kph. So when I hit 25 or 30 on my bike, or 50 last
week on my friend’s motorbike, it is a serious thrill.)

3) Dairy. This might be a non-sequitur, but I never really appreciated
raw dairy before. You never know what to expect. It’s a little
different every time, depending on the exact sequence of aging (or
not) and boiling (or not) and separating (or not). I want to do like
the apocryphal eskimo and coin forty different words for milk.

4) GNH Stats. You may know that Bhutan is governed by Gross National
Happiness rather than GDP (and this has some substance, despite the
hype). This means that the government does detailed surveys of every
locality in the country to make decisions, and that means there are a
wealth of stats available. Some interesting tidbits: I live in the
least happy place in the country. We also have the most drinking—the
average age to start drinking in my local area is 13.4. My theory is
that they asked about happiness when everyone was hungover. Or maybe
there is another, less optimistic connection…

5) Amateur Art and Engineering. Buddhism comes with an impulse for
artistry. Life is meaningless suffering, so you might as well decorate
it, right? Every house is decorated with paintings and colors and
symbols, the landscape is decorated with flags and prayer wheels, and
there is a lot of pride in the decoration of everything hand-crafted
(clothing and oboes in my neighborhood). There is also a lot of
engineering geared towards spreading goodwill. There are wind-powered
prayer wheels and water-powered prayer wheels. Our librarian rigged up
a little prayer wheel outside the library from a juice bottle and some
kind of broken walkman. And, in case you think Buddhism doesn’t have a
sense of humor, penises are featured in a lot of the art. Penises with
faces and personalities and sometimes wings. Look up “the Divine
Madman” for that story. Also on this topic, you can build anything
with bamboo. Every time there’s a local festival, a bamboo and tarp
village materializes within a day.

That’s all I’ve got for now. As always, I miss you all. Please tell me
what you are up to, any communication is savored here. I will write
again after this break, which should give me a lot of new material to
share.

Love, Danny “Thubten Zhenden” Wangdi (Danny for ease of pronunciation,
Thubten for “leans on buddha”, Zhenden for “likes everyone”, Wangdi
for “father of dynasties”. These last three were given to me, and I
take them all as aspirational titles.)
