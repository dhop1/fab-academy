---
title: "Logic Meets Intuition"
date: 2021-01-29
draft: false
categories: [science]
tags: [ai]
favorite: false
---
## Neural Networks Start to Think Like Humans

Neural networks---the turbocharged engines behind miraculous innovations like GPT-3, DALL-E, and AlphaFold---can not yet do arithmetic. They can't even count. They struggle to decide what comes after 63, 64, 65. A neural network, lacking appropriate assumptions, can justify 8.3 as easily as 66.

Neural networks excel at tasks that mirror human intuition---tasks that are based on extensive prior experience and involve millions or billions of inscrutable computations. But humans don't count using intuition or past experience. We can continue indefinitely, even into uncharted realms where no one has ever counted before.

Human cognition combines powerful intuition with the logic of language and mathematics. We can examine the complex patterns of our own intuitions and find simpler patterns underlying them. A dog and a dolphin can catch a ball, but only a human can discover the logic of a parabola and predict where a rocket booster will land before it is launched. In other words, we can distill our intuitive understanding into a simplified, symbolic *model* of reality.

According to Max Tegmark, a physicist and futurist, it is critical that we endow Artificial Intelligence with this power of introspection, one component of what he calls *intelligible intelligence*, ``AI that you can trust because you can understand its reasoning.''

In a first step towards that goal, Tegmark and a doctoral student at MIT's Center for Brains, Minds and Machines, Silviu-Marian Udrescu, developed a model that they call AI Feynman. AI Feynman is capable of examining its own intuition of a physical system to determine the symbolic logic governing that system. For example, given a dataset whose samples contain the masses of two objects, their spatial coordinates and the resulting force between them, AI Feynman can recover the law of gravitational attraction.

AI Feynman uses a neural network to learn the intuition of a system, which allows it to interpolate between the given data points and test for symmetries and other simplifications. It uses intuition to test logical propositions.

However, the neural network component of AI Feynman does not introspect independently---it is only a component of a larger algorithm that includes hand-coded logic. That means there is still a lot of work to do. Humans perform intuition and logic within a single network of neurons---the brain.

Another breakthrough effort towards integrating intuition and logic is MuZero, a new algorithm from DeepMind, published in *Nature* in December 2020. Unlike its famous game-playing AlphaGo and AlphaZero, MuZero can master games from Chess to Asteroids without being given the rules. Instead, it builds its own simplified model of reality while playing. Like a novice human, MuZero learns what to pay attention to and what to ignore when it makes predictions.

Without a model, prediction is immensely complex and data-hungry. With a model, MuZero is able to learn with relatively modest computational resources such as you might find on a home computer. Compare that to GPT-3---a language algorithm without underlying language logic---whose training consumed at least enough electricity to power 50 British homes for a year.

Unlike AI Feynman, MuZero's entire system is contained in its neural networks. However---also unlike AI Feynman---it is not able to output the logic of its world-model in a human-interpretable (symbolic) form. The model remains cryptically embedded in the numerical "hidden states" and "state transitions" of its own network.

These approaches point to two future directions for AI. Algorithms like AI Feynman will leverage their intuitive power to explain complex systems in a way that humans can understand. Those like MuZero will use theirs to perform complex operations that are beyond human understanding. Neural networks can't count yet, but don't count them out.
