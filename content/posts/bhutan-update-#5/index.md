---
title: "Bhutan Update #5"
slug: "bhutan-update-5"
date: 2017-02-18
draft: false
categories: [travel]
tags: []
favorite: false
---

{{< gallery
    match="images/*"
    sortOrder="asc"
    loadJQuery="true"
    embedPreview="true"
    resizeOptions="600x600 q90 Lanczos"
    previewType="blur"
    showExif="false"
>}}

*Note: this was originally sent from my posting in Gongthung, Tashigang, Bhutan. I don't agree with everything I said back then, but I'm pasting it here unedited as a kind of time capsule.*

Dear Everyone,

First of all, thank you for your responses to my last email. Your kind words helped me avoid neurotic spirals during my meditation retreat. I will respond to your emails soon. Until then, here’s the latest and greatest from my side.

I had planned to tell you about Japan, where it’s acceptable and safe to sleep on the floor of a highway rest area and where I was scolded for crossing empty roads without a walk sign, scolded for folding blankets incorrectly, and scolded for wearing the wrong slippers in the wrong room. Then I wanted to tell you about Thailand, where I volunteered to move dirt with the loveliest people, and I wanted to put in a plug for budget traveling with workaway.info (really, dot info). But then, I decided instead to tell you about Burma, where I found myself in a war zone alongside a bumbling Mr. Kurtz and a militant Captain Fantastic, beset by mortar fire, old (mostly extinct) landmines, snakes, scorpions, and European Rambo wannabes, and where I dispersed the first funds from the Burlington Green Bag Fund before escaping under cover of bronchitis.

But all that had to be shelved when I returned to Bhutan, because then I was eager to tell you about how my gho helped me conceal 15 pounds of baggage at the airport, and how I spent a month staying with friends and friends of friends, and—in the case of my biggest adventure—with my friend’s student’s boyfriend’s brother-in-law, who saved me from the jungle, where I was stumbling through nettles fleeing the shining eyes of tigers and leopards (which were, in retrospect, probably horses). And how on that particular adventure, into remote Kheng, I was turned around—for lack of route permit—to face the leopards and tigers again, this time overnight, and how I stayed in a cave, hiding behind a fire and singing loudly to send the following stern warning to all beasts: “if you come, I will bash you with a ukulele.”

Then I had to table those stories, because I went for a Vipassana meditation retreat and I desperately wanted to share that misery with you. So desperately that, by day six, I was digging through trash cans searching for a pen to write notes on my chest. And also to write the outline of the Hercule Poirot murder mystery that was blossoming in my mind: “Silent But Deadly”, inspired by the fact that there is a surprising amount of audible farting (and burping and snoring) during fifteen hours per day of collective silence. But I also wanted to share that the meditation really affected my perception of pain and (importantly, given the current flea population on my ankles) itching.

But for now, all of that can wait, because I arrived two weeks ago at my new home, Gongthung Middle Secondary School. And now, I only want to share my excitement about starting my next chapter here. There are already some major differences from last year. The school is bigger and higher-achieving and the teachers go to class at least 30% more and make even longer and more tedious speeches at the uncomprehending students. Most importantly, I have discarded my most unwieldy cultural baggage: the desire “not to impose” on people; this time around, I am filling my karmic-ledger with debits to be repaid later. A big lesson here (and I guess a lesson of poverty in general) is that every instance of generosity and every debt made and repaid brings a community closer together. A village is built on mutual dependence. As my language ability improves, I’m pondering a move into one of the surrounding villages, where I could really immerse myself in culture (and distilled beverages).

I will not wait so long before writing again. I miss you all, and I hope winter is either mild or severe, depending on your personal climatic preferences.

Dan

Attachments

1: Descending a volcano above the Sea of Japan

2: Fishermen in Karen state, Myanmar, where kids wield M16s in the world’s longest-running war…

3: …and the site of our first Burlington Green Bag Fund donation, to help the 8 adopted children in this hoveliest of hovels. Sadly, they are much better off than the hundreds of thousands of Karen refugees in Thailand.

4: The valley whose siren song beckoned me all year from my desktop background, route permit be damned…

5: …and the beautiful villages that awaited at the end of a long day’s hike….

6: …and the cave that sheltered me from ferocious felines on my return north.

(If you look closely—and if you’re as nerdy as me—you can match the second-to-last ridge on the left in \#4 with the second-to-last ridge on the left in \#5, proving that a long day’s hike in the Himalayas does not take you very far.)

7: The view of Gongthung, my new home, looking uphill from school. I waited nearly two weeks for the forest fires to subside so I could get a good photo of the views, but--despite my heroic patience--they stubbornly burn on.
