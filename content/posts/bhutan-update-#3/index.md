---
title: "Bhutan Update #3"
slug: "bhutan-update-3"
date: 2015-10-03
draft: false
categories: [travel]
tags:
favorite: false
---

{{< gallery
    match="images/*"
    sortOrder="desc"
    loadJQuery="true"
    embedPreview="true"
    resizeOptions="600x600 q90 Lanczos"
    previewType="blur"
    showExif="false"
>}}

*Note: this was originally sent from my posting in Tsebar, Pemagatshel, Bhutan. I don't agree with everything I said back then, but I'm pasting it here unedited as a kind of time capsule.*

Dear friends,

It’s five in the morning and I was just awakened by an earthquake. I
feel like I’ve been at Tsebar for about three years now (that’s the
longevity multiplier that makes traveling such a good deal. And it
means I miss you all three times more than you miss me.). In my three
(apparent) years here, things have changed in the village: the number
of cars seems to have almost doubled and mobile internet has arrived.
The former means more frequent archery matches with nearby villages.
The latter means that the teachers can now share the joys of the
internet age, such as: the internet not working, the internet working
slowly, the internet working only if you stand in the right spot
behind the kitchen, searching for low-quality porn, getting viruses
from low-quality porn, and being completely taken in by every fake
video and rumor on Facebook. All of the above are even accessible in
class, if you have the proper attitude towards teaching.

It is apple, pear, guava and maize season now. Maize season means that
the hills are dotted with little fires at night, where people are
camping in stilt-huts to protect their crops from monkeys, wild boars
and porcupines. In the evening, people are shouting and banging drums
everywhere to deter these pests. (I had a proud moment recently, when
I was the first to spot a pack of boars wreaking havoc in a field.
Also, have you ever tasted porcupine? Pretty tasty. It’s almost the
only meat I have seen this year.) [Update: The school pig died
tragically yesterday, so meat is temporarily available, but no one
really wants to eat it. We had an hour of very poignant prayer to ease
his soul’s transition.]
{{< audio src="/media/bhutan/PrayerForThePig.m4a" >}}

The monsoon has abruptly finished. The official end was September
22nd, Blessed Rainy Day, when the heavens do their washing and it
rains holy water every year. It rained every single day in August,
usually heavily. We didn’t see the sun for three weeks, all of the
roads were blocked for two weeks, and the power was out for one week.
And I found an intrepid leech that made it to my second-floor rooms. I
used to think leeches were kind of adorable, moving like blind
inchworms towards the nearest blood source, but they lost my support
last week. I took two walks on trails that are not used this season
and discovered why. After the first, my left pant leg was so bloody I
looked like I had been practicing my amateur amputation skills. And
after the second, I took off my shoe and discovered, in one square
inch, fifteen leeches trying to burrow through my sock in an orgiastic
melee that would give Indiana Jones nightmares. I mention this to
impress you with how beautiful these walks are: leeches are a fair
price for a walk through pristine cloud forest on ancient,
history-laden, chorten-dotted paths.
<br><br>

***

<br>
Here’s a short adventure catalog of the past three months:
<br><br>

### Summer break
Two weeks with the other “canadian” teachers spent exploring, playing
games, and purging our brains of every pent-up philosophy, pun, and
problem of the previous five months. We spent the time mostly walking
in Merak-Sakteng, a pair of high-altitude valleys that are home to
ethnically-Tibetan yak-herders, and Bumthang, the religious heart of
Bhutan and also its “Switzerland”, named for its pine forests, beer
and emmental cheese. NB: Hitch-hiking in Bhutan is the best. It often
comes with a free meal, or at least drinks.

### Walk to Assam
My friend Adam and I took advantage of a long weekend (as in, two
days) to walk to India. An older teacher had told me about the old
trade route. When he was young, the whole valley used to head to Assam
for the winter, walking for three days with the year’s produce of
maize, potatoes and oranges on horse and human backs. He said that
after returning, laden with rice, they would party until the rice ran
out and then get back to work.

We were told that we would surely be bled dry by leeches on this
route, after which our desiccated corpses would certainly be attacked
by bears and trampled by elephants. We are used to these kind of dire
warnings now, and we sang songs to deter the bears (as for elephants,
what more auspicious way to die?). The trails were beautiful, all
credit to the unconnected villages en route. The villagers, not having
roads, keep their trails immaculate. The walk to the border town
(Nanglam) took about 14 hours, including stops when villagers gave us
tea, lunch, milk (this one from a hut high in a tree, apparently in an
elephant-prone area) and cold whey (this one at a much-needed swimming
hole). We were counting on people’s generosity and we brought gifts
for our benefactors.

The view of the plains of Assam from the mountains is stunning. It is
an incredible, impossible-seeming geological contrast. The “foothills”
of the Himalayas don’t fade gradually; they just stop.

*Notable cultural moment*: I met a man from my village coming the other
way and asked him what he had been doing in Nanglam. He said shopping.
I noticed that he was carrying nothing but an umbrella. That gives you
a sense of how time is valued here: it’s worth a two-day walk each way
to save a few cents on an umbrella. Of course, I did the same walk and
bought nothing, so…

*Another note*: In mountain-themed movies, people are always crossing
death-defying rope bridges over chasms. Those bridges at least have
stood the test of time; try crossing a raging waterfall on a
forty-foot swaying bamboo span that looks like someone slapped it
together last week.

We had no plan at all for our return the next day, but of course,
things worked out. Some old men decided to drive us about four hours
(most of the way home). And they brought tea and biscuits to mark the
occasion.

One other note from this trip. We reached a village called Nomey.
Nomey was the first unconnected village I ever saw. Soon after
arriving at Tsebar, I was eight hours into a walk, exhausted and a
little lost, standing next to two ancient chortens high on a ridge,
and I spotted its glinting tin roofs. I started sobbing. I felt
nostalgia for something that I have never known, but which my dna
recognized right away. So I was excited to finally reach it. I was
half-hoping, but not really expecting, to find people barely scraping
out an impoverished and joyless existence, so that I could finally
quash the “noble savage” notion that makes teaching an ambivalent job
for me. No such luck. The people were kind and happy, the houses were
well-built and the farms seemed productive. So teaching continues to
be ambivalent. (Nobody is at school here for the “joy of learning”;
learning is no fun in this school system. They are at school being
incessantly beaten and scolded because they have been promised a
better life and they trust that promise.)

### Fiddleheads
I went with some students to a place called Kapter the other day. This
is the kind of place that makes local knowledge an expensive thing to
lose. Literally. It is an otherwise unremarkable little clearing in
the jungle that happens to be an amazing place to collect mushrooms
and fiddleheads. The four of us collected nearly 150 pounds in an
hour, with time leftover for exploring and picnic lunch.

### Tsechus, lhaseys, rimdros, tikkus and variety shows
Starting about a couple weeks ago and continuing through the end of
the year, holidays and teaching have traded places: holidays and
special occasions are the dominant lifeform, and teaching is an
inconvenient interruption. Each village will have a Tsechu, a three
day festival of games, drinking and masked dances. Each family will
also have its own festival, called a Lhasey. There are also rituals
called Rimdros for every conceivable reason. One student’s family just
had a three day Rimdro to honor a prayer wheel that they built four
years ago. It’s also the season for important Lamas and Rinpoches to
move from their summer to winter residences. When one of these passes
our way, we wait on the side of the road for half the day in order to
receive his blessing (called a Tikku, which means being hit in the
head by an assortment of holy objects). I am an innocent bystander for
all of these occasions, but I am working hard for our variety show.
One of my goals for this year was to learn more about video editing,
so I have been doing a lot of work with a green screen (i.e. blue bed
sheet) to make music videos for the show.
<br><br>
***
<br>
I spent the last month agonizing about whether to stay or go. Deficit
of adult friends has stopped the roulette wheel on go, but I’m afraid
that I’m making a big mistake by leaving behind a job that I’m excited
to go to. I regularly stay at school from seven in the morning to
almost seven in the evening. During my free periods, I mope around
hoping that a teacher will fail to show up so that I can take an extra
class. Is it crazy to throw that away? There’s a lot of growth that
can happen when a person is free of shoulds and coulds. And life is
healthy here, so the opportunity cost of a year here might be nil. In
good buddhist form, I’ve chosen the middle path between staying and
leaving: I’ll stay for December and January to teach at a winter camp
for poor girls. Then I will start to mosey home.

<br><br>
Love,
Dan
